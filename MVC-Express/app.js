const express = require('express');
const { engine } = require('express-handlebars');
const app = express();
const userRoutes = require('./routes/user');


app.engine('hbs', engine({ extname: '.hbs', defaultLayout: 'main' }));
app.set('view engine', '.hbs');
app.use(express.urlencoded({ extended: true }));
app.use(userRoutes)


app.listen(8000, () => {
    console.log('Server is running');
})