const { getUser } = require("../models/userModel")

exports.login = (req, res, next) => {
    res.render('login', {
        docTitle: 'Login'
    })
}

exports.dashboard = (req, res, next) => {
    const user = getUser(req.query.email);
    res.render('dashboard', {
        docTitle: 'Dashboard',
        user
    })
}

exports.loginProcess = (req, res, next) => {
    const user = getUser(req.body.email);
    if (user !== null && user.password === req.body.password) {
        res.redirect('/home?email=' + user.email)
    } else if (user === null) {
        res.render('error', {
            message: ' User Not Found!.. Please Sign Up.'
        });
    } else {
        res.render('error', {
            message: ' Invalid Credentials...'
        });
    }
}
