const express = require('express');
const page = require('../controllers/userController')
const router = express.Router();

router.get('/', page.login)
router.get('/home', page.dashboard)
router.post('/login', page.loginProcess)

module.exports = router;