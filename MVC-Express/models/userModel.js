const fs = require('fs');
const path = require('path');

exports.getUser = (email) => {
    const buffered_data = fs.readFileSync(path.join(__dirname, '../data/', 'user.json'));
    const jsonData = JSON.parse(buffered_data);

    const filteredUser = jsonData.filter((user) => {
        return user.email == email
    })
    if (filteredUser.length > 0) {
        // console.log(filteredUser[0]);
        return filteredUser[0];
    }
    return null;
}