const express = require('express')
const app = express()
const path = require('path')
const multer = require('multer')

app.set('views', path.join(__dirname, "views"));
app.set('view engine', 'ejs');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads');
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname.replace(/\.[^/.]+$/, "") + '_' + Date.now() + path.extname(file.originalname));
    }
});

let maxSize = 2 * 1000 * 1000

let upload = multer({
    storage: storage,
    limits: {
        fileSize: maxSize,
    },
    fileFilter: function (req, file, cb) {
        let filetypes = /jpeg|jpg|png/;
        let mimetype = filetypes.test(file.mimetype);
        let extname = filetypes.test(path.extname(file.originalname).toLowerCase());

        if (mimetype && extname) {
            return cb(null, true);
        }

        cb('Error: Upload' + filetypes + ' only!', false);
    }
}).single('image')

app.get('/', (req, res) => {
    res.render("signup")
})

app.post('/upload', (req, res, next) => {
    upload(req, res, (err) => {
        if (err) {
            if (err instanceof multer.MulterError && err.code == "LIMIT_FILE_SIZE") {
                return res.send('File size should be max 2mb')
            }
            res.send(err);
        } else {
            res.send("Success... Image Uploaded!")
        }
    })
})

app.listen(8000, () => {
    console.log('server is running')
})