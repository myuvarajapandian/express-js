const express = require('express');
const app = express();
const bodyparser = require('body-parser');
const exhbs = require('express-handlebars');
const dbo = require('./db');
const objectID = dbo.objectID;


app.engine('hbs', exhbs.engine({
    layoutsDir: 'views/layouts',
    defaultLayout: 'main',
    extname: '.hbs'
}));
app.set('view engine', 'hbs');
app.set('views', 'views');
app.use(bodyparser.urlencoded({ extended: true }));

app.get('/', async (req, res) => {
    let database = await dbo.getDatabase();
    const collection = database.collection('books');
    let books = await dbo.getCollection('books');
    console.log(books)
    let message = '';

    if (req.query.delete_id) {
        delete_id = req.query.delete_id;
        await collection.deleteOne({ _id: new objectID(delete_id) })
        return res.redirect('/?status=3');
    }
    switch (req.query.status) {
        case '1':
            message = 'Book Inserted successfully!...';
            break;
        case '2':
            message = 'Book Updated Successfully!...';
            break;
        case '3':
            message = 'Book Deleted Successfully!...';
            break;
        default:
            break;
    }
    res.render('index', { message, books, docTitle: "Books", title: 'books' });
})

app.get('/create', async (req, res) => {
    let books = await dbo.getCollection('books');

    res.render('form', { books, docTitle: "Create" });
})

app.get('/update', async (req, res) => {
    let database = await dbo.getDatabase();
    const collection = database.collection('books');
    let books = await dbo.getCollection('books');


    let edit_id, edit_book;
    if (req.query.edit_id) {
        edit_id = req.query.edit_id;
        edit_book = await collection.findOne({ _id: new objectID(edit_id) });
    }

    res.render('form', { edit_id, edit_book, books, docTitle: "Update" });
})

app.post('/create', async (req, res) => {
    let database = await dbo.getDatabase();
    const collection = database.collection('books');
    let book = {
        title: req.body.title,
        author: req.body.author
    }
    await collection.insertOne(book);
    return res.redirect('/?status=1');
})

app.post('/update/:edit_id', async (req, res) => {
    let database = await dbo.getDatabase();
    const collection = database.collection('books');
    let book = {
        title: req.body.title,
        author: req.body.author
    }
    let edit_id = req.params.edit_id;
    await collection.updateOne({ _id: new objectID(edit_id) }, { $set: book });
    return res.redirect('/?status=2');
})

app.listen(8000, () => {
    console.log('server is running');
})