const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;
const objectID = mongodb.ObjectId;

let database;

async function getDatabase() {
    const client = await MongoClient.connect('mongodb://localhost:27017/');
    database = client.db('library');

    if (!database) {
        console.log('Error connecting to database')
        return "Error connecting to database";
    }
    return database;
}

async function getCollection(col_name) {
    let db = await getDatabase();
    const collection = db.collection(col_name);

    if (!collection) {
        console.log('Error connecting to collection')
        return "Error fetching to collection";
    }
    const cursor = collection.find({});
    let data = await cursor.toArray();
    // console.log(data)
    return data;
}
module.exports = { getDatabase, objectID, getCollection }