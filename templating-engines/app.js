const express = require('express');
const app = express();
const router = express.Router();
const path = require('path');
const rootDir = require('./utils/path')
const { engine } = require('express-handlebars');
const { exists } = require('fs');
const courses = [
    "JavaScript",
    "Express.js",
    "Node.js",
    "PHP"
]

app.use(express.static(path.join(__dirname, 'public')))

app.engine('hbs', engine({ extname: '.hbs', defaultLayout: 'main' }))

app.set('view engine', 'hbs')

router.get('/', (req, res) => {
    res.status(200).render('index', {
        docTitle: "Welcome To Express.js",
        courses: courses,
        path: '',
        course_exists: courses.length > 0,
        pageIndex: true
    });
})
router.get('/about', (req, res) => {

    res.status(200).render('about', {
        docTitle: "About",
        courses: courses,
        path: 'about',
        course_exists: courses.length > 0,
        pageAbout: true
    })
})

router.use((req, res, next) => {
    res.status(200).render('404', {
        docTitle: "Page Not Found",
        path: '404',
        page404: true
    })
})

app.use(router);

app.listen(8000, () => {
    console.log('Running on 8000')
})